FROM node:lts-alpine

RUN mkdir -p /home/node/gymManager/node_modules && chown -R node:node /home/node/gymManager

WORKDIR /home/node/gymManager

COPY package.json yarn.* ./

USER node

RUN yarn install --production

COPY --chown=node:node . .

EXPOSE 3000

CMD ["node", "server.js"]
