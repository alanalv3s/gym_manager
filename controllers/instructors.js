const fs = require('fs')
const data = require('../data.json')
const { age, date } = require('../utils')

exports.index = function (req, res) {
    const response = data.instructors.map((instr) => {
        const instructor = {
            ...instr,
            services: instr.services.split(',')
        }
        return instructor;
    })

    return res.render("instructors/index", { instructors: response })
}

exports.create = function (req, res) {
    return res.render("instructors/create")
}

exports.post = function (req, res) {

    const keys = Object.keys(req.body)

    for (key of keys) {
        // req.body.key == ""
        if (res.send[key] = "")
            return res.send('Por favor, preencha todos os campos.')
    }

    let { avatar_url, birth, name, services, gender } = req.body // Destructuring

    birth = Date.parse(birth)
    const created_at = Date.now()
    const id = Number(data.instructors.length + 1)

    data.instructors.push({
        id,
        avatar_url,
        name,
        birth,
        gender,
        services,
        created_at,
    })

    fs.writeFile("data.json", JSON.stringify(data, null, 2), function (err) {
        if (err) return res.send("Erro em escrever o arquivo!")

        return res.redirect("/")
    })

    // return res.send(req.body)
}

exports.show = function (req, res) {
    const { id } = req.params

    const foundInstructor = data.instructors.find(function (instructor) {
        return instructor.id == id
    })
    if (!foundInstructor) return res.send("Instrutor não encontrado!")

    const instructor = {
        ...foundInstructor,
        age: age(foundInstructor.birth),
        services: foundInstructor.services.split(","),
        created_at: new Intl.DateTimeFormat('en').format(foundInstructor.created_at),
    }

    return res.render("instructors/show", { instructor })
}

exports.edit = function (req, res) {
    const { id } = req.params

    const foundInsctructor = data.instructors.find(function (instructor) {
        return id == instructor.id
    })
    if (!foundInsctructor) return res.send("Instrutor não encontrado!")

    const instructor = {
        ...foundInsctructor,
        birth: date(foundInsctructor.birth).iso
    }

    date(foundInsctructor.birth)

    return res.render('instructors/edit', { instructor })
}

exports.put = function (req, res) {
    const { id } = req.body
    let index = 0

    const foundInsctructor = data.instructors.find(function (instructor, foundIndex) {
        if (id == instructor.id) {
            index = foundIndex
            return true
        }
    })

    if (!foundInsctructor) return res.send("Instrutor não encontrado!")

    const instructor = {
        ...foundInsctructor,
        ...req.body,
        birth: Date.parse(req.body.birth),
        id: Number(req.body.id)
    }

    data.instructors[index] = instructor

    fs.writeFile("data.json", JSON.stringify(data, null, 2), function (err) {
        if (err) return red.send("Erro na escrita dos dados!")

        return res.redirect(`/instructors/${id}`)
    })
}

exports.delete = function (req, res) {
    const { id } = req.body

    const filteredInstructors = data.instructors.filter(function (instructor) {
        return instructor.id != id
    })

    data.instructors = filteredInstructors

    fs.writeFile("data.json", JSON.stringify(data, null, 2), function (err) {
        if (err) return res.send("Erro ao deletar o instrutor!")

        return res.redirect("/instructors")
    })
}